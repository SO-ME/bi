#include<graphics.h>
#include<conio.h>
#include<math.h>
#define High 480
#define Width 640
#define PI 3.14159
int main()
{
   initgraph(Width,High);
    int center_x,center_y;//中心坐标，也是秒针的起始坐标
   center_x=Width/2;
   center_y=High/2;

   int secondLength=Width/5;//秒针长度
   int minuteLength=Width/6;
   int hourLength=Width/7;
   
   int secondEnd_x,secondEnd_y;//秒针的终点坐标
   int minuteEnd_x,minuteEnd_y; //分钟的长度
   int hourEnd_x,hourEnd_y;   //时针的长度
  
   float secondAngle=0;//秒针对应转动角度
   float minuteAngle;   //分针对应转动角度
   float hourAngle;    //时针对应转动角度

   SYSTEMTIME ti;  //定义变量存储系统时间
   

     while(1)
   {
  

        GetLocalTime(&ti);  //获取当前的时间


	    //秒钟角度变化
	   secondAngle = ti.wSecond * 2*PI/60;//一圈2*PI，一圈60秒，一秒钟秒针走过的角度为2*PI/60
       
       //分钟角度变化
	   minuteAngle=ti.wMinute*2*PI/60;//一圈共2*PI，一圈60分，一分钟分针走过的角度为2*PI/60

	   //时针角度变化
	   hourAngle=ti.wHour*2*PI/12;//一圈共2*PI，一圈12小时，一小时时针走过的角度为2*PI/12

	   //由角度决定的秒针端点坐标
     secondEnd_x=center_x+secondLength*sin(secondAngle);
     secondEnd_y=center_y-secondLength*cos(secondAngle);

	 //由角度决定的分针端点坐标
       minuteEnd_x=center_x+minuteLength*sin(minuteAngle);
	   minuteEnd_y=center_y-minuteLength*cos(minuteAngle);

	   //由角度决定的时针端点坐标
	      hourEnd_x=center_x+hourLength*sin(hourAngle);
	   hourEnd_y=center_y-hourLength*cos(hourAngle);



   //画秒针
   setlinestyle(PS_SOLID,2);
   setcolor(WHITE);
   line(center_x,center_y,secondEnd_x,secondEnd_y);
  
   
    //画分针
   setlinestyle(PS_SOLID,4);
   setcolor(BLUE);
   line(center_x,center_y,minuteEnd_x,minuteEnd_y);

    //画时针 
   setlinestyle(PS_SOLID,6);
   setcolor(RED);
   line(center_x,center_y,hourEnd_x,hourEnd_y);
  


     Sleep(10);


	//隐藏前面一帧的秒针
	 setcolor(BLACK);
	 setlinestyle(PS_SOLID,2);
	line(center_x,center_y,secondEnd_x,secondEnd_y);

	//隐藏前面一帧的分针
    setlinestyle(PS_SOLID,4);
    line(center_x,center_y,minuteEnd_x,minuteEnd_y);

	//隐藏前面一帧的时针
	 setlinestyle(PS_SOLID,6);
     line(center_x,center_y,hourEnd_x,hourEnd_y);
  
	 }
   getch();	  
   closegraph();
   return 0;
}
